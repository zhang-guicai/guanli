import { createApp, VueElement } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './assets/base.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import * as echarts from 'echarts';
// console.log(echarts);

// import api from './api/index.js'
// Vue.prototype.$api=api
// Vue.config.productionTip=false
// App.prototype.$api=api
// App.config.productionTip=false
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// app.config.globalProperties.$api = api
app.config.productionTip=false
app.use(ElementPlus)
app.use(router)
app.mount('#app')
